#!/bin/bash

#[ -d "/dev/ttyACM0"  ] && rm /dev/ttyACM0
#ln -s /dev/ttyS1 /dev/ttyACM0

file="$1"
if [ $# -eq 0 ]
  then
  file="main"
fi

bazel run app:_${file}Upload --platforms @AvrToolchain//platforms:ArduinoUno