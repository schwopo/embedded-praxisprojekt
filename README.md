# Release
### Das ist neu:
    -MQTT Client
    -Integration Tests für nebenläufige Aufgaben und Polling
    -Option für Polling im Serial Modul
    -Sleep Mode: Idle für 1ms
    -Dokumentation für alle Interfaces und Commands
    -Größe der Task Queue wird im Header definiert

### Features des MQTT Clients:
    -Publishing von Outputs des Arduinos ins /output Subtopic
    -Weitergeben von Commands an den Arduino über /commands
    -Publishing in definierbare Subtopics über !{Subtopic} {Value} (Beispiel: echo !planet earth)
    -Weitergeben von globalen Commands, Konfigurierbar durch /subscribe_all (0, 1)

### Anwendung Thermostat:
    -Starten mit enableThermostat = 1 im MQTT root
    -Durchschnitt aller Arduino Temperaturen wird genommen
    -Wenn 24°C erreicht sind, schalte LED (Klimaanlage) an



# How to run
### Upload:  
Upload main.c: ./upload.sh main  
Upload echo.c: ./upload.sh echo  
etc  

### MQTT Client:
python3 mqtt/mqtt.py

### Integration Tests:  
./run-tests.sh > results  

Lädt automatisch Testcode hoch und führt Integration Test Scripts aus.  
Bazel Uploads spammen den stdout zu, daher in File pipen.  

