#!/bin/bash

echo Running Tests:

echo Uploading echo.c ...
./upload.sh echo
echo Done.
echo

python3 test-integration/test-serial.py

echo Uploading echo_polling.c ...
./upload.sh echo_polling
echo Done.
echo

python3 test-integration/test-serial.py

echo
echo Uploading tasktest.c ...
./upload.sh tasktest
echo Done.
echo

python3 test-integration/test-task.py
echo

echo
echo Uploading schedulecommand.c ...
./upload.sh schedulecommand
echo Done.
echo

python3 test-integration/test-schedule-command.py
echo

echo
echo Uploading schedulecommand.c ...
./upload.sh schedulecommand
echo Done.
echo

python3 test-integration/test-schedule-command.py
echo

echo
echo Uploading tasktest_multitask.c ...
./upload.sh tasktest_multitask
echo Done.
echo

python3 test-integration/test-schedule-multitask.py
echo