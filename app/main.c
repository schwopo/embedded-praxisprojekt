#include <stdio.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "../libs/bootcamp/pwm.h"
#include "../libs/bootcamp/temp.h"
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/timer.h"
#include "../libs/bootcamp/serial.h"
#include "../libs/bootcamp/command.h"
#include "../libs/bootcamp/fastsine.h"
#include "../libs/bootcamp/task_queue.h"
#include "../libs/bootcamp/circularBuffer.h"

pwm_t pwm = NULL;
int sine_levels = 16;
int sine_wavelength_ms = 1000;
int counter;

void print_counter (char *arg) {
	serial_printf("%d\n", counter);
}

void increment_counter (char *arg) {
	counter++;
}

void send_temp(void *_) {
	int temp = temp_get();
	serial_printf("%d\n", temp);
}

void pub_temp(void *_) {
	int temp = temp_get();
	serial_printf("!temp %d\n", temp);

}

void toggle_led(char *arg) {
	PORTB ^= _BV(5);
}

void led_on() {
	PORTB = _BV(5);
}

void led_off() {
	PORTB = 0;
}

void print_cmd_list(char *arg) {
	char buffer[128];
	command_list(buffer);
	serial_printf("%s", buffer);
}

void print_echo(char *arg) {
	serial_printf("%s", arg);
}

void led_write(char *arg) {
	int v;
	sscanf(arg, "%d", &v);
	if(v)
		PORTB = _BV(5);
	else
		PORTB = 0;
}

void led_flash(char *arg) {
	int v;
	sscanf(arg, "%d", &v);
	for (int i = 0; i < v; i++) {
		task_queue_add_task(task_queue_time + 500*i, 0, led_write, "1");
		task_queue_add_task(task_queue_time + 500*i + 50, 0, led_write, "0");
	}
}

void tasker_add(char *arg) {
	int start, repeat;
	int consumed;
	sscanf(arg, "%d %d %n", &start, &repeat, &consumed);

	int cmd_length = strlen(arg + consumed);
	char *cmd_copy = malloc(cmd_length);
	strcpy(cmd_copy, arg+consumed);

	serial_printf("adding task: start in: %d ms, repeat all: %dms\n", start, repeat);
	int task_id = task_queue_add_task(task_queue_time+start, repeat, command_interpret, cmd_copy);
	task_queue_free_context_on_finish(task_id);
	serial_printf("task id: %d ", task_id);
	serial_printf("command: %s\n", cmd_copy);
}

void tasker_remove(char *arg) {
	int id;
	sscanf(arg, "%d", &id);
	task_queue_remove_task(id);
}

void led_sine(char *arg) {
	sscanf(arg, "%d", &sine_wavelength_ms);
	pwm_start(pwm);
}

void apply_sine(void *whatev) {
		int compareValue = (sine_levels/2) + (sine_levels/2) * sine( (float) task_queue_time / sine_wavelength_ms);
		pwm_set_compare_value(pwm, compareValue);
}

void led_sine_off(char *arg) {
	pwm_stop(pwm);
	led_off();
}


int main () {
	serial_init();

	temp_init();
	temp_ts_gain = 128;
	temp_ts_offset = 20;

	DDRB = _BV(5);

	setUpTimerForOneMillisecond();
	sei();

	//prints a list of all available commands
	command_add("help", print_cmd_list);

	//prints its parameter
	command_add("echo", print_echo);

	//led 1 to turn on, led 0 to turn off
	command_add("led", led_write);

	//toggles led
	command_add("toggle", toggle_led);

	//flash led n times
	command_add("flash", led_flash);

	//increment global counter
	command_add("inc", increment_counter);

	//print global counter
	command_add("counter", print_counter);

	//print internal temperature
	command_add("temp", send_temp);

	//print temperature in a way the mqtt client knows to publish
	command_add("pubtemp", pub_temp);

	//task add: ta <miliseconds from now> <miliseconds to repeat> <command> <parameters...>
	//prints a task id
	command_add("ta", tasker_add);

	//task remove: tr <id>
	command_add("tr", tasker_remove);

	//enable sine function on led
	//sine <wavelength in ms>
	command_add("sine", led_sine);

	//disable sine on led
	command_add("unsine", led_sine_off);

	task_queue_add_task(0, 20, apply_sine, NULL);

	pwm = pwm_create(sine_levels, led_on, led_off);
	pwm_set_compare_value(pwm, 0);

	char readline_buffer[82] = {0};
	int a, b;

	while(1) {
		task_queue_poll();

		if (uart_lines > 0) {
			serial_readline(readline_buffer);
			serial_printf(">%s", readline_buffer);
			command_interpret(readline_buffer);
		}

		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_enable();
		power_adc_disable();
		sleep_mode();
		sleep_disable();
		power_adc_enable();
	}
}

ISR(TIMER0_COMPA_vect){
	task_queue_time++;
	pwm_step(pwm);
}

ISR(USART_RX_vect) {
	uart_receive();
}

ISR(USART_TX_vect) {
	uart_transmit();
}

ISR(ADC_vect) {
	temp_isr();
}