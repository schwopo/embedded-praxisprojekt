#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/circularBuffer.h"
#include "../libs/bootcamp/task_queue.h"
#include "../libs/bootcamp/temp.h"
#include "../libs/bootcamp/timer.h"
#include "../libs/bootcamp/serial.h"
#include "../libs/bootcamp/command.h"
#include <inttypes.h>
#include <string.h>
#include <stdio.h>

void print_cmd_list(char *arg) {
	char buffer[128];
	command_list(buffer);
	serial_printf("%s", buffer);
}

void print_echo(char *arg) {
	serial_printf("%s", arg);
}

int main () {
	serial_init();

	setUpTimerForOneMillisecond();
	sei();

	command_add("help", print_cmd_list);
	command_add("echo", print_echo);

	char readline_buffer[82] = {0};
	int a, b;

	while(1) {
		task_queue_poll();
		uart_poll();
		if (uart_lines > 0) {
			serial_readline(readline_buffer);
			serial_printf(">%s", readline_buffer);
			command_interpret(readline_buffer);
		}
	}
}

ISR(TIMER0_COMPA_vect){
	task_queue_time++;
}

ISR(USART_RX_vect) {
	uart_receive();
}

ISR(USART_TX_vect) {
	uart_transmit();
}

ISR(ADC_vect) {
	temp_isr();
}