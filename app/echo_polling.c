#include <avr/io.h>
#include <avr/interrupt.h>
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/serial.h"
#include <inttypes.h>

int main () {
	serial_init_no_interrupts();
	sei();
	char readline_buffer[82] = {0};
	while(1) {
		if (uart_lines > 0) {
			serial_readline(readline_buffer);
			serial_printf("%s", readline_buffer);
		}
		uart_poll();
	}
}