#include <avr/io.h>
#include <avr/interrupt.h>
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/serial.h"
#include "../libs/bootcamp/timer.h"
#include "../libs/bootcamp/task_queue.h"
#include <inttypes.h>

void send_task(void *arg) {
    char *msg = arg;
    serial_printf("%s", msg);
}

int main () {
	setUpTimerForOneMillisecond();
	task_queue_add_task(0, 800, send_task, "hallo\n");
	int id = task_queue_add_task(0, 30, send_task, "XXXXXX\n");
    task_queue_remove_task(id);
    
	serial_init();
	sei();
	char readline_buffer[70] = {0};
	while(1) {
        task_queue_poll();
	}
}

ISR(USART_RX_vect) {
	uart_receive();
}

ISR(USART_TX_vect) {
	uart_transmit();
}

ISR(TIMER0_COMPA_vect){
	task_queue_time++;
}