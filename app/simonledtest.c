#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/circularBuffer.h"
#include "../libs/bootcamp/task_queue.h"
#include "../libs/bootcamp/temp.h"
#include "../libs/bootcamp/timer.h"
#include "../libs/bootcamp/serial.h"
#include "../libs/bootcamp/command.h"
#include "../libs/bootcamp/fastsine.h"
#include <inttypes.h>
#include <string.h>
#include <stdio.h>

volatile int counter;

void print_counter (char *arg) {
	serial_printf("%d\n", counter);
}

void increment_counter (char *arg) {
	counter++;
}

void send_temp(void *_) {
	int temp = temp_get();
	serial_printf("%d\n", temp);

}

void toggle_led(char *arg) {
	PORTB ^= _BV(5);
}

void print_cmd_list(char *arg) {
	char buffer[128];
	command_list(buffer);
	serial_printf("%s", buffer);
}

void print_echo(char *arg) {
	serial_printf("%s", arg);
}

void led_write(char *arg) {
	int v;
	sscanf(arg, "%d", &v);
	if(v)
		PORTB = _BV(5);
	else
		PORTB = 0;
}

void led_flash(char *arg) {
	int v;
	sscanf(arg, "%d", &v);
	for (int i = 0; i < v; i++) {
		task_queue_add_task(task_queue_time + 500*i, 0, led_write, "1");
		task_queue_add_task(task_queue_time + 500*i + 50, 0, led_write, "0");
	}
}

void tasker_add(char *arg) {
	int start, repeat;
	int consumed;
	sscanf(arg, "%d %d %n", &start, &repeat, &consumed);

	int cmd_length = strlen(arg + consumed);
	char *cmd_copy = malloc(cmd_length);
	strcpy(cmd_copy, arg+consumed);

	serial_printf("adding task: start in: %d ms, repeat all: %dms\n", start, repeat);
	serial_printf("command: %s\n", cmd_copy);
	int task_id = task_queue_add_task(task_queue_time+start, repeat, command_interpret, cmd_copy);
	task_queue_free_context_on_finish(task_id);
	serial_printf("task id: %d\n", task_id);
}

void tasker_remove(char *arg) {
	int id;
	sscanf(arg, "%d", &id);
	task_queue_remove_task(id);
}

int compareValue = 255;
int flag = 0;
void adjust_compare(char *arg) { 
	if(!flag)
		compareValue++;
	else
		compareValue--;

	if(compareValue > 255)
		flag = 1;
	if(compareValue < 0);
		flag = 0;
}

int main() {
	sei();
	setUpTimerForOneMillisecond();
	DDRB = _BV(5);
	while (1) {
		compareValue = 10 + 10 * sine( (float) task_queue_time / 200.f);
	}
}

ISR(TIMER0_COMPA_vect){
	task_queue_time++;
		counter += 1;
		if (counter >= compareValue) {
			PORTB = 0;
		}
		if (counter >= 20) {
			PORTB = _BV(5);
			counter = 0;
		}
}

ISR(USART_RX_vect) {
	uart_receive();
}

ISR(USART_TX_vect) {
	uart_transmit();
}

ISR(ADC_vect) {
	temp_isr();
}