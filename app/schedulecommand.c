#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include "../libs/bootcamp/uart.h"
#include "../libs/bootcamp/circularBuffer.h"
#include "../libs/bootcamp/task_queue.h"
#include "../libs/bootcamp/temp.h"
#include "../libs/bootcamp/timer.h"
#include "../libs/bootcamp/serial.h"
#include "../libs/bootcamp/command.h"
#include <inttypes.h>
#include <string.h>
#include <stdio.h>


void print_echo(char *arg) {
	serial_printf("%s", arg);
}


void tasker_add(char *arg) {
	int start, repeat;
	int consumed;
	sscanf(arg, "%d %d %n", &start, &repeat, &consumed);

	int cmd_length = strlen(arg + consumed);
	char *cmd_copy = malloc(cmd_length);
	strcpy(cmd_copy, arg+consumed);

	serial_printf("adding task: start in: %d ms, repeat all: %dms\n", start, repeat);
	serial_printf("consumed: %d chars\n", consumed);
	serial_printf("command: %s\n", cmd_copy);
	int task_id = task_queue_add_task(task_queue_time+start, repeat, command_interpret, cmd_copy);
	task_queue_free_context_on_finish(task_id);
	serial_printf("task id: %d\n", task_id);
}

int main () {
	serial_init();

	setUpTimerForOneMillisecond();
	sei();

	command_add("echo", print_echo);

	int task_id = task_queue_add_task(0, 800, command_interpret, "echo hallo\n");

	char readline_buffer[82] = {0};
	int a, b;

	while(1) {
		task_queue_poll();
		//uart_poll();
		if (uart_lines > 0) {
			serial_readline(readline_buffer);
			serial_printf(">%s", readline_buffer);
			command_interpret(readline_buffer);
		}
	}
}

ISR(TIMER0_COMPA_vect){
	task_queue_time++;
}

ISR(USART_RX_vect) {
	uart_receive();
}

ISR(USART_TX_vect) {
	uart_transmit();
}

ISR(ADC_vect) {
	temp_isr();
}