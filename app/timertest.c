#include <avr/io.h>
#include <avr/interrupt.h>

int timer = 0;

void setup()
{

  TCCR0A = (1 << WGM01); //Set the CTC mode
  OCR0A = 0xF9;          //Value for ORC0A for 1ms

  TIMSK0 |= (1 << OCIE0A); //Set the interrupt request
  sei();                   //Enable interrupt

  TCCR0B |= (1 << CS01); //Set the prescale 1/64 clock
  TCCR0B |= (1 << CS00);
}

ISR(TIMER0_COMPA_vect)
{ 
  timer++;
}

int main()
{
  setup();

  while (1)
  {
    DDRB = _BV(5);
    if (timer >= 1000)
    {
      PORTB ^= _BV(5);
      timer = 0;
    }
  }
}