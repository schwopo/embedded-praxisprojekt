import serial

print ("testing small input")

s = serial.Serial("/dev/ttyACM0", timeout=1.0)
result = s.readline()

out = b"hallo\n"

#test fails on first run, send a line without testing
s.write(out)
s.flush()
result = s.readline()

s.write(out)
s.flush()
result = s.readline()
assert(result==out)
print ("pass")


print ("testing big input")

out = b"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"

s.write(out)
s.flush()
result = s.readline()
assert(result==out)
print ("pass")
