#include "bootcamp/sine.h"
#include <math.h>
#include <inttypes.h>

float sine_derivate_at_0(int);
uint64_t factorial(uint64_t n);
float sine_taylor_term(float, int );

float sine(float x){
    x = fmod(x, 2*M_PI);

    if (x > M_PI)
        x = x - 2*M_PI;
    if (x < -M_PI)
        x = x + 2*M_PI;

    int taylor_limit = 20;
    float taylor_sum = 0;
    for (int n = 0; n < taylor_limit; n++){
        taylor_sum += sine_taylor_term(x, n);
    }
    return taylor_sum;
}

float sine_taylor_term(float x, int n) {
    return (sine_derivate_at_0(n) / (float) factorial(n)) * pow(x-0, n);
}

float sine_derivate_at_0(int n) {
    //sin, cos, -sin, -cos
    float derivates[] = {0, 1, 0, -1};
    return derivates[n%4];
}

uint64_t factorial(uint64_t n) {
    if (n == 0)
        return 1;
    else
        return n*factorial(n-1);
}