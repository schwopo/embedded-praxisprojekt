#include "bootcamp/command.h"
#include "bootcamp/serial.h"
#include "bootcamp/temp.h"
#include <string.h>
#include <stdio.h>

int cmd_help(char *args);
int cmd_echo(char *args);
int cmd_temp(char *args);

typedef struct {
    char* name;
    void (*command) (char *);
} cmd_list_entry;

cmd_list_entry cmd_list[] = {
    {"help", cmd_help},
    {"echo", cmd_echo},
    {"temp", cmd_temp}
};

int cmd_num = (sizeof cmd_list) / (sizeof cmd_list[0]);

int command_execute(char *command) {
    //cursed code
    char command_name[10];
    sscanf(command, "%s", command_name);
    int comparison;
    int found_command = 0;

   for (int i = 0; i < cmd_num; i++) {
       comparison = strcmp(command_name, cmd_list[i].name);
       if(comparison == 0) {
           cmd_list[i].command(command + strlen(command_name));
           found_command = 1;
       }
   }
    if(!found_command){
        serial_printf("Command not found.\n");
        return -1;
    } else {
        return 0;
    }
}

int cmd_help(char *args) {
    serial_printf("List of Commands:\n");
   for (int i = 0; i < cmd_num; i++) {
       serial_printf("%s\n",cmd_list[i].name);
   }
   return 0;
}

int cmd_echo(char *args) {
    serial_printf("%s", args);
    return 0;
}

int cmd_temp(char *args) {
    int temp = temp_get();
    serial_printf("Temperature: %d Celsius\n", temp);
    return 0;
}