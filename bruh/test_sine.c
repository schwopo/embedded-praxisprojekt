#include "bootcamp/sine.h"
#include <math.h>
#include "unity.h"

float sine_derivate_at_0(int);
uint64_t factorial(uint64_t n);
float sine_taylor_term(float, int);

const float sine_acceptable_delta = 0.00001f;

void test_sine_0() {
    TEST_ASSERT_EQUAL_FLOAT(0, sine(0));
}

void test_sine_pi_multiples() {
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 1, sine(M_PI_2));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(M_PI));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-M_PI));
}

void test_sine_edge_cases() {
    float edge_distance = 0.00000001f;
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(2*M_PI - edge_distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-2*M_PI + edge_distance));
}

void test_sine_big_input() {
    float distance = 20 * 2 * M_PI;
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 1, sine(M_PI_2 + distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(M_PI + distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2 + distance));
    
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2 - distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-M_PI + distance));
}

void test_sine_antisymmetry() {
    float step_size = 0.001f;
    for(float x = 0; x < M_PI; x += step_size){
        TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(x)+sine(-x));
    }
}

void test_sine_compare_math_h_sin() {
    float step_size = 0.001f;
    for(float x = 0; x < M_PI; x += step_size){
        TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, sin(x), sine(x));
    }
}

void test_factorial() {
    TEST_ASSERT_EQUAL(1, factorial(0));
    TEST_ASSERT_EQUAL(1, factorial(1));
    TEST_ASSERT_EQUAL(120, factorial(5));
}

void test_sine_taylor_term_at_0() {
    TEST_ASSERT_EQUAL_FLOAT(0, sine_taylor_term(0, 0));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_taylor_term(0, 1));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_taylor_term(0, 2));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_taylor_term(0, 3));
}

void test_sine_taylor_term_at_1() {
    TEST_ASSERT_EQUAL_FLOAT(0, sine_taylor_term(1, 0));
    TEST_ASSERT_EQUAL_FLOAT(1, sine_taylor_term(1, 1));
}

void test_sine_derivate_at_0(){
    TEST_ASSERT_EQUAL_FLOAT(0, sine_derivate_at_0(0));
    TEST_ASSERT_EQUAL_FLOAT(1, sine_derivate_at_0(1));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_derivate_at_0(2));
    TEST_ASSERT_EQUAL_FLOAT(-1, sine_derivate_at_0(3));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_derivate_at_0(4));
    TEST_ASSERT_EQUAL_FLOAT(1, sine_derivate_at_0(5));
    TEST_ASSERT_EQUAL_FLOAT(0, sine_derivate_at_0(6));
    TEST_ASSERT_EQUAL_FLOAT(-1, sine_derivate_at_0(7));
}