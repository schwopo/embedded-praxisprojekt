#include "bootcamp/task_queue.h"
#include <stdlib.h>
#include <string.h>

typedef struct task {
    uint64_t start;
    uint64_t repeat;
    void (*fn) (void *);
    void *context;
    char free_on_finish;
} task;

task task_queue[TASK_QUEUE_SIZE];
volatile uint64_t task_queue_time;

int task_queue_add_task(uint64_t start, uint64_t repeat, void (*fn) (void *), void *context) {
    for (int i = 0; i < TASK_QUEUE_SIZE; i++) {
        if(!task_queue[i].fn) {
            task_queue[i].start = start;
            task_queue[i].repeat = repeat;
            task_queue[i].fn = fn;
            task_queue[i].context = context;
            return i;
        }
    }
}

void task_queue_free_context_on_finish(int id) {
    task_queue[id].free_on_finish = 1;
}

void task_queue_remove_task(int task_id) {
        task_queue[task_id].fn = 0;
        if(task_queue[task_id].free_on_finish)
            free(task_queue[task_id].context);
}

void task_queue_poll() {
    for (int i = 0; i < TASK_QUEUE_SIZE; i++) {
        if(task_queue[i].fn) {
            if (task_queue_time >= task_queue[i].start) {
                //run task fn with its context
                task_queue[i].fn(task_queue[i].context);

                //set new start time for repeat or remove
                if (task_queue[i].repeat > 0) {
                    task_queue[i].start = task_queue[i].start + task_queue[i].repeat;
                } else {
                    task_queue_remove_task(i);
                }
            }
        }
    }
}

void task_queue_clear() {
    for (int i = 0; i < TASK_QUEUE_SIZE; i++) {
        if(task_queue[i].fn) {
            task_queue_remove_task(i);
        }
    }
    memset(task_queue, 0, sizeof task_queue);
}