#include "bootcamp/uart.h"
#include "bootcamp/avr/io.h"

#define FOSC 16000000 // Clock Speed
#define BAUD 9600
#define MYUBRR (FOSC/16/BAUD-1)

static int8_t (*uart_readf) (void *, uint8_t *);
static int8_t (*uart_writef) (void *, uint8_t);

static void *uart_rbuf;
static void *uart_wbuf;

volatile int uart_lines = 0;

void USART_Transmit(uint8_t data){
    //required so that we can send large text without killing the buffer
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)))
        ;
    /* Put data into buffer, sends the data */
    UDR0 = data;
}

int USART_Receive(uint8_t *data){
    if ((UCSR0A & (1<<RXC0))) {
         *data = UDR0;
         return 1;
    }
    else return 0;
}


int8_t uart_read(uint8_t *out_data) {
    uint8_t data;
    int8_t return_code = uart_readf(uart_rbuf, &data);
    *out_data = data;

    if(data == '\n')
        uart_lines--;

    return return_code;
}

int8_t uart_write(uint8_t data) {
    int8_t return_code = uart_writef(uart_wbuf, data);
    return return_code;
}

void uart_init(void *readbuf, void *writebuf, int8_t (*readfunc) (void *, uint8_t *),int8_t (*writefunc) (void *, uint8_t)) {
    /*Set baud rate */
    const unsigned int ubrr = MYUBRR;
    UBRR0H = (unsigned char)(ubrr>>8);
    UBRR0L = (unsigned char)ubrr;

    /* Enable receiver and transmitter */
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);

    /* Set frame format: 8data, 2stop bit */
    UCSR0C = (1<<USBS0)|(3<<UCSZ00);

    uart_readf = readfunc;
    uart_writef = writefunc;

    uart_rbuf = readbuf;
    uart_wbuf = writebuf;
}

void uart_poll() {
    uart_receive();
    uart_transmit();
}

void uart_enable_interrupts() {
    UCSR0B |= (1<<RXCIE0);
    UCSR0B |= (1<<TXCIE0);
}

void uart_receive(){
    uint8_t readdata;
    if(USART_Receive(&readdata))
        uart_writef(uart_rbuf, readdata);

    if(readdata == '\n')
        uart_lines++;
}

void uart_transmit(){
    uint8_t data;
    if (uart_readf(uart_wbuf, &data) == 0)
        USART_Transmit(data);
}