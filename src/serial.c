#include "bootcamp/uart.h"
#include "bootcamp/circularBuffer.h"
#include <stdio.h>
#include "bootcamp/vsscanf.h"


#define RBUFSIZE 128
#define WBUFSIZE 128

uint8_t read_buffer_data[RBUFSIZE];
uint8_t write_buffer_data[WBUFSIZE];

static void serial_uart_out (char *s) {
	while(*s) {
		while(uart_write(*s) < 0)
            ;
        s++;
	}
}

int serial_readline(char *buffer) {
    int i = 0;
    char read_char;

    do {
        uart_read(&read_char);
        buffer[i] = read_char;
        i++;
    } while (read_char != '\n');

    buffer[i] = 0;
    return 0;
}

int serial_init() {
    serial_init_no_interrupts();
	uart_enable_interrupts();
    return 0;
}

int serial_init_no_interrupts() {
    circularBuffer_t read_buffer = circularBuffer_init(read_buffer_data, RBUFSIZE);
    circularBuffer_t write_buffer = circularBuffer_init(write_buffer_data, WBUFSIZE);
	uart_init(read_buffer, write_buffer, circularBuffer_read, circularBuffer_push);
    return 0;
}

int serial_printf(char *format_string, ...) {
    char buffer[82];
    
    va_list args;
    va_start(args, format_string);
    vsprintf(buffer, format_string, args);
    va_end(args);

    serial_uart_out(buffer);
    uart_transmit();
    
    return 0;
}

int serial_scanf(char *format_string, ...) {
    char buffer[82];
    serial_readline(buffer);

    va_list args;
    va_start(args, format_string);
    vsscanf(buffer, format_string, args);
    va_end(args);

    return 0;
}
