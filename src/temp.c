#include "bootcamp/temp.h"
#include "bootcamp/avr/io.h"

static int last_adc_measurement;
int temp_ts_offset = 0;
int temp_ts_gain = 128;

void temp_init(){ 
    ADCSRA = (1<<ADEN);
    ADCSRA |= (1<<ADSC);
    ADCSRA |= (1<<ADIE);
    ADCSRA |= (1<<ADATE);
    ADCSRA |= (1<<ADPS2);
    ADCSRA |= (1<<ADPS1);

    ADMUX = (1 << REFS1);
    ADMUX |= (1 << REFS0);
    ADMUX |= (1<<MUX3);
}

int temp_convert_celsius(int reading, int ts_offset, int ts_gain) {
    float ts_gain_ratio = 128.f / (float) ts_gain;
    return (reading - (273 + 100 - ts_offset) * ts_gain_ratio) + 25;
}

int temp_get(){
    return temp_convert_celsius(last_adc_measurement, temp_ts_offset, temp_ts_gain);
}

void temp_isr() {
    last_adc_measurement = ADCW;
}