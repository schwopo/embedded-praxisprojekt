#include "bootcamp/pwm.h"
#include <stdlib.h>

typedef struct pwm_type {
    int maxvalue;
    int compareValue;
    volatile int step;
    void (*on_fn)();
    void (*off_fn)();
    int running;
} pwm_type;

typedef pwm_type* pwm_t;

pwm_t pwm_create(int max, void (*on_fn)(), void (*off_fn)()) {
    pwm_t p = malloc(sizeof (pwm_type)); 
    p->maxvalue = max;
    p->step = 0;
    p->compareValue = 0;
    p->on_fn = on_fn;
    p->off_fn = off_fn;
    p->running = 0;
    return p;
}

void pwm_set_compare_value(pwm_t pwm, int compare_value) {
    pwm->compareValue = compare_value;
}

void pwm_step(pwm_t pwm) {
    if (pwm == NULL)
        return;

    if (!pwm->running)
        return;

    if(pwm->step == 0) {
        pwm->on_fn();
    }

    if(pwm->step == pwm->compareValue) {
        pwm->off_fn();
    }


    pwm->step = (pwm->step + 1) % pwm->maxvalue;
}

void pwm_start(pwm_t pwm) {
    pwm->running = 1;
}

void pwm_stop(pwm_t pwm) {
    pwm->running = 0;
}