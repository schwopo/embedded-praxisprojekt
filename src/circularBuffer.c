#include "bootcamp/circularBuffer.h"
#include <stdlib.h>

typedef struct circularBuffer {
    uint8_t* buffer;
    size_t size;
    int read_head;
    int write_head;
} circularBuffer;

typedef circularBuffer* circularBuffer_t;

circularBuffer_t circularBuffer_init(uint8_t* buffer, size_t size) {
    circularBuffer *cb = malloc(sizeof (circularBuffer));
    cb->buffer = buffer;
    cb->size = size;
    cb->read_head = 0;
    cb->write_head = 0;
    return cb;
}

void circularBuffer_free(circularBuffer_t cbuf) {
    free(cbuf);
}

static void zero_out_buffer(circularBuffer_t cbuf) {
    for (int i = 0; i<cbuf->size; i++){
        cbuf->buffer[i] = 0;
    }
}

void circularBuffer_reset(circularBuffer_t cbuf){
    zero_out_buffer(cbuf);
    cbuf->read_head = 0;
    cbuf->write_head = 0;
}

void circularBuffer_overwrite(circularBuffer_t cbuf, uint8_t data){
    cbuf->buffer[cbuf->write_head] = data;
    cbuf->write_head = (cbuf->write_head + 1) % cbuf->size;
}

int8_t circularBuffer_push(circularBuffer_t cbuf, uint8_t data) {
    //fail on full buffer
    if (circularBuffer_full(cbuf)) {
        return -1;
    }

    cbuf->buffer[cbuf->write_head] = data;
    cbuf->write_head = (cbuf->write_head + 1) % cbuf->size;
    return 0;
}

int8_t circularBuffer_read(circularBuffer_t cbuf, uint8_t* data) {
    if(circularBuffer_empty(cbuf)){
        return -1;
    }

    *data = cbuf->buffer[cbuf->read_head];
    cbuf->read_head = (cbuf->read_head + 1) % cbuf->size;
    return 0;
}

bool circularBuffer_empty(circularBuffer_t cbuf) {
    return cbuf->write_head == cbuf->read_head;
}

bool circularBuffer_full(circularBuffer_t cbuf){
    return (cbuf->write_head+1)%cbuf->size == cbuf->read_head;
}

size_t circularBuffer_capacity(circularBuffer_t cbuf) {
    return cbuf->size;
}

//% operator does not work as modulo on negative numbers
//therefore define modulo
//https://stackoverflow.com/a/42131603
static int modulo(int x,int N) {
    return (x % N + N) %N;
}

size_t circularBuffer_size(circularBuffer_t cbuf) {
    return modulo((cbuf->write_head) - (cbuf->read_head), cbuf->size);
}