#include "bootcamp/fastsine.h"
#include <math.h>

//include one padding value
float sine_values_to_pi[] = {
    0.0,
    0.15643446504,
    0.309016994375,
    0.45399049974,
    0.587785252292,
    0.707106781187,
    0.809016994375,
    0.891006524188,
    0.951056516295,
    0.987688340595,
    1.0,
    0.987688340595,
    0.951056516295,
    0.891006524188,
    0.809016994375,
    0.707106781187,
    0.587785252292,
    0.45399049974,
    0.309016994375,
    0.15643446504,
    0.0,
    -0.15643446504
};

int sine_num_values = (sizeof sine_values_to_pi / sizeof sine_values_to_pi[0]) - 1;

float lerp(float a, float valx, float valy) {
    return a * valx + (1-a) * valy;
}

float sine(float x) {
    int invert_result=0;
    x = fmod(x, 2*M_PI);

    if (x > M_PI)
        x = x - 2*M_PI;
    if (x < -M_PI)
        x = x + 2*M_PI;

    if (x < 0) {
        x += M_PI;
        invert_result=1;
    }

    float normalized_position_in_table = x / M_PI;
    float index_position = normalized_position_in_table * (sine_num_values-1);

    int index = floor(index_position);
    float a = 1 - (index_position - floor(index_position));

    float result = lerp(a,sine_values_to_pi[index], sine_values_to_pi[index+1]);
    //float result = sine_values_to_pi[index];
    if(invert_result)
        result *= -1;

    return result;
}

