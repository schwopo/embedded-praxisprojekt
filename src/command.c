#include "bootcamp/command.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define COMMAND_MAX 20
typedef struct command {
    char *name;
    void (*fn)(char *);
} command;

static command commands[COMMAND_MAX];
static int commands_num = 0;

static command *command_find(char *name) {
    for (int i = 0; i < commands_num; i++) {
        command *c = commands + i;
        if(strcmp(c->name, name) == 0) {
            return c;
        }
    }
    return NULL;
}

void command_list(char *buffer) {
    for (int i = 0; i < commands_num; i++) {
        command *c = commands + i;
        if(c->name == NULL) {
            break;
        }
        buffer += sprintf(buffer, "%s\n", c->name);
    }
}

void command_add(char *name, void (*fn) (char *)) {
    if(strchr(name, " ") != NULL) {
        //don't accept space in command names
        return;
    }

    command *c = command_find(name);

    if (c == NULL) {
        c = commands + commands_num;
        commands_num++;
    }
    
    c->name = name;
    c->fn = fn;
}

int command_execute(char *name, char *args) {
    command *c = command_find(name);
    if (c == NULL) {
        return -1;
    }
    (c->fn) (args);
    return 0;
}

int command_interpret(char *str) {
    char cmd_name[20] = {0};
    sscanf(str, "%s", cmd_name);

    char *args = strchr(str, ' ');
    if(args == NULL) {
        args = "";
    } else {
        //go after the space
        args++;
    }

    return command_execute(cmd_name, args);
}

int command_exists(char *name) {
    return command_find(name) != NULL;
}

void command_clear() {
    memset(commands, 0, sizeof commands);
    commands_num = 0;
}