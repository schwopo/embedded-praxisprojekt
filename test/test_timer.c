#include "unity.h"
#include "bootcamp/timer.h"
#include "bootcamp/avr/interrupt.h"

uint8_t TCNT0;
uint8_t OCR0A;
uint8_t TCCR0A;
uint8_t TCCR0B;
uint8_t TIMSK0;

void test_setUpTimerForOneMillisecond() {
    setUpTimerForOneMillisecond();

    TEST_ASSERT_EQUAL(0, TCNT0);
    TEST_ASSERT_EQUAL(250, OCR0A);

	TEST_ASSERT_EQUAL((1<<WGM01), TCCR0A);
	TEST_ASSERT_EQUAL((1<<OCIE0A), TIMSK0);
	TEST_ASSERT_EQUAL((1<<CS01)|(1<<CS00), TCCR0B);
}