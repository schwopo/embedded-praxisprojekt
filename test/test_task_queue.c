#include "unity.h"
#include "bootcamp/task_queue.h"


int ctx;
void incctx (void *c) {
    int *ptr = (int *) c;
    (*ptr)++;
}

void reset() {
    task_queue_clear();
    task_queue_time = 0;
}

void test_task_queue_add_task_fn_call() {
    reset();
    ctx = 0;
    
    task_queue_add_task(1, 0, incctx, &ctx);
    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);
    task_queue_time = 1;
    task_queue_poll();
    TEST_ASSERT_EQUAL(1, ctx);
}

void test_task_queue_add_task_repeat() {
    reset();
    ctx = 0;
    
    task_queue_add_task(1, 1, incctx, &ctx);
    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);

    task_queue_time = 1;
    task_queue_poll();
    TEST_ASSERT_EQUAL(1, ctx);

    task_queue_time = 2;
    task_queue_poll();
    TEST_ASSERT_EQUAL(2, ctx);
}

void test_task_queue_remove_task() {
    reset();
    ctx = 0;
    
    int id = task_queue_add_task(1, 0, incctx, &ctx);
    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);

    task_queue_remove_task(id);

    task_queue_time = 1;
    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);
}

void test_task_queue_clear() {
    reset();
    ctx = 0;
    
    task_queue_add_task(1, 0, incctx, &ctx);
    task_queue_add_task(1, 0, incctx, &ctx);
    task_queue_add_task(1, 0, incctx, &ctx);

    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);

    task_queue_clear();

    task_queue_time = 1;
    task_queue_poll();
    TEST_ASSERT_EQUAL(0, ctx);
}