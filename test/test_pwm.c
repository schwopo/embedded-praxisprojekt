#include "unity.h"
#include "bootcamp/pwm.h"

int led;

void led_on(){
    led = 1;
}

void led_off(){
    led = 0;
}

void test_pwm_step() {
    led_off();
    pwm_t pwm = NULL;

    pwm_step(pwm); //just check for segfault

    pwm = pwm_create(4, led_on, led_off);
    pwm_start(pwm);
    pwm_set_compare_value(pwm, 2);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    //next_cycle

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);
}

void test_pwm_start_stop() {
    led_off();
    pwm_t pwm = NULL;
    pwm = pwm_create(4, led_on, led_off);
    pwm_set_compare_value(pwm, 2);
    TEST_ASSERT_EQUAL(0, led);

    //check running before start
    
    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

   // start 
    pwm_start(pwm);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    //check stop
    pwm_stop(pwm);
    pwm_step(pwm);
    TEST_ASSERT_EQUAL(1, led);

    //continue
    pwm_start(pwm);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

}

void test_pwm_cmpv_zero() {
    led_off();
    pwm_t pwm = NULL;

    pwm_step(pwm); //just check for segfault

    pwm = pwm_create(4, led_on, led_off);
    pwm_start(pwm);
    pwm_set_compare_value(pwm, 0);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

    pwm_step(pwm);
    TEST_ASSERT_EQUAL(0, led);

}