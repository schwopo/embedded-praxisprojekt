#include "unity.h"
#include "bootcamp/command.h"
#include <string.h>
#include <stdio.h>

char sc_buf[80];

void simple_command(char *arg) {
    strcpy(sc_buf, arg);
}

void test_command_list () {
    command_clear();
    char buffer[128] = {0};
    char buffer2[128] = {0};
    //empty list
    command_list(buffer);
    TEST_ASSERT_EQUAL_STRING("", buffer);

    command_add("test", simple_command);
    command_list(buffer);
    TEST_ASSERT_EQUAL_STRING("test\n", buffer);

    command_add("test", simple_command);
    command_add("test", simple_command);
    command_add("test", simple_command);
    command_list(buffer2);
    TEST_ASSERT_EQUAL_STRING(buffer, buffer2);
}

void test_command_add_diff_name () {
    command_clear();
    command_add("code", printf);
    command_add("cmd", simple_command);
}

void test_command_exists() {
    command_clear();
    int return_on_not_found = command_exists("fake");
    TEST_ASSERT_EQUAL(0, return_on_not_found);

    command_add("write", simple_command);
    int return_on_found = command_exists("write");
    TEST_ASSERT_EQUAL(1, return_on_found);
}

void test_command_clear() {
    command_clear();
    command_add("write", simple_command);
    int return_on_found = command_exists("write");
    TEST_ASSERT_EQUAL(1, return_on_found);

    command_clear();

    int return_on_not_found = command_exists("write");
    TEST_ASSERT_EQUAL(0, return_on_not_found);

}

void test_command_add_reject_space_names () {
    command_clear();
    command_add("my cmd", simple_command);
}

void test_command_execute() {
    command_clear();
    char *arg = "hallo";

    int return_on_not_found = command_execute("fake", arg);
    TEST_ASSERT_EQUAL(-1, return_on_not_found);

    command_add("write", simple_command);

    int return_on_found = command_execute("write", arg);
    TEST_ASSERT_EQUAL_STRING(arg, sc_buf);
    TEST_ASSERT_EQUAL(0, return_on_found);
}

void test_command_interpret() {
    command_clear();
    command_add("test", simple_command);
    int return_on_not_found = command_interpret("fake");
    int return_on_found = command_interpret("test");
    TEST_ASSERT_EQUAL(-1, return_on_not_found);
    TEST_ASSERT_EQUAL(0, return_on_found);

    command_add("write", simple_command);
    simple_command("empty");
    command_interpret("write hello");

    TEST_ASSERT_EQUAL_STRING("hello", sc_buf);
}

void test_command_full_capacity() {
    command_clear();
    command_add("test1", simple_command);
    command_add("test2", simple_command);
    command_add("test3", simple_command);
    command_add("test4", simple_command);
    command_add("test5", simple_command);
    command_add("test6", simple_command);
    command_add("test7", simple_command);
    command_add("test8", simple_command);
    command_add("test9", simple_command);
    command_add("test10", simple_command);
    command_add("test11", simple_command);
    command_add("test12", simple_command);
    command_add("test13", simple_command);
    command_add("test14", simple_command);
    command_add("test15", simple_command);
    command_add("test16", simple_command);
    command_add("test17", simple_command);
    command_add("test18", simple_command);
    command_add("test19", simple_command);
    command_add("test20", simple_command);
    TEST_ASSERT_EQUAL(1, command_exists("test20"));
    TEST_ASSERT_EQUAL(1, command_exists("test1"));
}