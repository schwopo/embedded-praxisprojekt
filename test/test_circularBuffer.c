#include "unity.h"
#include "bootcamp/circularBuffer.h"
#include "inttypes.h"

void test_circularBuffer_init(){
    int size = 2000;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    TEST_ASSERT_NOT_NULL(cb);
}

void test_circularBuffer_free(){
    int size = 2000;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_free(cb);
    //TEST?????
    TEST_PASS();
}

void test_circularBuffer_overwrite(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_overwrite(cb, 1);
    circularBuffer_overwrite(cb, 2);
    circularBuffer_overwrite(cb, 3);
    circularBuffer_overwrite(cb, 4);
    circularBuffer_overwrite(cb, 5);

    uint8_t expected_vals[] = {5, 2, 3, 4};
    TEST_ASSERT_EQUAL_INT8_ARRAY(expected_vals, buffer, size);
}

void test_circularBuffer_push(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_push(cb, 1);
    circularBuffer_push(cb, 2);
    int ec_before_full = circularBuffer_push(cb, 3);
    int ec_on_full = circularBuffer_push(cb, 4);

    TEST_ASSERT_EQUAL(0, ec_before_full);
    TEST_ASSERT_EQUAL(-1, ec_on_full);

    uint8_t expected_vals[] = {1, 2, 3, 0};
    TEST_ASSERT_EQUAL_INT8_ARRAY(expected_vals, buffer, size);

}

void test_circularBuffer_empty(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    bool empty_after_init = circularBuffer_empty(cb);
    TEST_ASSERT_EQUAL(1, empty_after_init);

    circularBuffer_push(cb, 1);
    bool empty_after_push = circularBuffer_empty(cb);
    TEST_ASSERT_EQUAL(0, empty_after_push);

    circularBuffer_reset(cb);
    bool empty_after_reset = circularBuffer_empty(cb);
    TEST_ASSERT_EQUAL(1, empty_after_reset);
}

void test_circularBuffer_read(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_push(cb, 1);
    circularBuffer_push(cb, 2);

    uint8_t data1;
    uint8_t data2;
    circularBuffer_read(cb, &data1);
    int ec_non_empty = circularBuffer_read(cb, &data2);

    TEST_ASSERT_EQUAL(1, data1);
    TEST_ASSERT_EQUAL(2, data2);
    TEST_ASSERT_EQUAL(0, ec_non_empty);

    int ec_empty = circularBuffer_read(cb, &data2);
    TEST_ASSERT_EQUAL(-1, ec_empty);
}

void test_circularBuffer_capacity(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    TEST_ASSERT_EQUAL(size, circularBuffer_capacity(cb));
}

void test_circularBuffer_size(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    TEST_ASSERT_EQUAL(0, circularBuffer_size(cb));

    circularBuffer_push(cb, 1);
    circularBuffer_push(cb, 1);
    circularBuffer_push(cb, 1);

    TEST_ASSERT_EQUAL(3, circularBuffer_size(cb));
}

void test_circularBuffer_size_overflow(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_overwrite(cb, 1);
    circularBuffer_overwrite(cb, 1);
    circularBuffer_overwrite(cb, 1);
    circularBuffer_overwrite(cb, 1);

    TEST_ASSERT_EQUAL(0, circularBuffer_size(cb));

    circularBuffer_overwrite(cb, 1);

    TEST_ASSERT_EQUAL(1, circularBuffer_size(cb));
}

void test_circularBuffer_with_read_greater_write(){
    int size = 4;
    uint8_t buffer[size];
    circularBuffer_t cb = circularBuffer_init(buffer, size);

    circularBuffer_overwrite(cb, 1);
    circularBuffer_overwrite(cb, 2);
    circularBuffer_overwrite(cb, 3);

    uint8_t data;
    circularBuffer_read(cb, &data);
    circularBuffer_read(cb, &data);
    circularBuffer_read(cb, &data);
    //read_head = 3

    circularBuffer_overwrite(cb, 4);
    circularBuffer_overwrite(cb, 5);
    //write_head == 5 (1)



    uint8_t buf_content[] = {5,2,3,4};
    TEST_ASSERT_EQUAL_INT8_ARRAY(buf_content, buffer, 4);

    TEST_ASSERT_EQUAL(2, circularBuffer_size(cb));

}

static int modulo(int x,int N) {
    return (x % N + N) %N;
}

void test_modulo() {
    TEST_ASSERT_EQUAL(2, modulo(1-3, 4));
}

void test_misc(){
}
