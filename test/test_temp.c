#include "unity.h"
#include "bootcamp/temp.h"
#include "bootcamp/avr/io.h"

uint8_t UDR0;

uint8_t UBRR0H;
uint8_t UBRR0L;

uint8_t UCSR0A; 
uint8_t UCSR0B; 
uint8_t UCSR0C; 

uint8_t ADMUX;
uint8_t ADCSRA;
uint8_t ADCW;

void test_temp_init() {
    temp_init();
    TEST_ASSERT_BIT_HIGH (ADEN, ADCSRA );
    TEST_ASSERT_BIT_HIGH (ADSC, ADCSRA );
    TEST_ASSERT_BIT_HIGH (ADIE, ADCSRA );
    TEST_ASSERT_BIT_HIGH (ADATE, ADCSRA );
    TEST_ASSERT_BIT_HIGH (ADPS2, ADCSRA );
    TEST_ASSERT_BIT_HIGH (ADPS1, ADCSRA );

    TEST_ASSERT_BIT_HIGH (REFS1, ADMUX );
    TEST_ASSERT_BIT_HIGH (REFS0, ADMUX );
    TEST_ASSERT_BIT_HIGH (MUX3, ADMUX );
}

void test_temp_convert(){
    TEST_ASSERT_EQUAL(52, temp_convert_celsius(400, 0, 128));
    TEST_ASSERT_EQUAL(152, temp_convert_celsius(500, 0, 128));
    TEST_ASSERT_EQUAL(52, temp_convert_celsius(500, -100, 128));
}

void test_temp_get() {
    int reading = 223;
    int ts_offset = 0;
    int ts_gain= 128;
    ADCW = reading;
    temp_isr();

    int temperature = temp_get();

    TEST_ASSERT_EQUAL(temp_convert_celsius(reading, ts_offset, ts_gain),temperature);
}