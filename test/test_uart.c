#include "bootcamp/uart.h"
#include "unity.h"
#include "bootcamp/avr/io.h"

#define FOSC  16000000// Clock Speed
#define BAUD 9600
#define MYUBRR (FOSC/16/BAUD-1)

const unsigned int ubrr = MYUBRR;

int rbuf;
int wbuf;

//uart io data register
uint8_t UDR0;

//baud register rate
uint8_t UBRR0H;
uint8_t UBRR0L;

//configuration registers
uint8_t UCSR0A; 
uint8_t UCSR0B; 
uint8_t UCSR0C; 

int read(int *buffer, uint8_t *data) {
    *data = 1;
    (*buffer)++;
    return 0;
}

int write(int *buffer, uint8_t data) {
    *buffer = data;
    return 0;
}

void test_uart_init_baud_rate() {
    uart_init(&rbuf, &wbuf, read, write);

    TEST_ASSERT_EQUAL((unsigned char) (ubrr>>8),UBRR0H);
    TEST_ASSERT_EQUAL((unsigned char) (ubrr),UBRR0L);
}

void test_uart_init_enable_tx() {
    uart_init(&rbuf, &wbuf, read, write);
    TEST_ASSERT_BIT_HIGH(TXEN0, UCSR0B);
}

void test_uart_init_enable_rx() {
    uart_init(&rbuf, &wbuf, read, write);
    TEST_ASSERT_BIT_HIGH(RXEN0, UCSR0B);
}

void test_uart_init_frame_format() {
    uart_init(&rbuf, &wbuf, read, write);
    TEST_ASSERT_BIT_HIGH(USBS0, UCSR0C);
    TEST_ASSERT_BIT_HIGH(UCSZ00, UCSR0C);
    TEST_ASSERT_BIT_HIGH(UCSZ01, UCSR0C);
}

void test_uart_init_interrupts_disabled() {
    uart_init(&rbuf, &wbuf, read, write);
    TEST_ASSERT_BIT_LOW(RXCIE0, UCSR0B);
    TEST_ASSERT_BIT_LOW(TXCIE0, UCSR0B);
}

void test_uart_enable_interrupts() {
    uart_init(&rbuf, &wbuf, read, write);
    uart_enable_interrupts();
    TEST_ASSERT_BIT_HIGH(RXCIE0, UCSR0B);
    TEST_ASSERT_BIT_HIGH(TXCIE0, UCSR0B);
}


void test_uart_read(){
    uart_init(&rbuf, &wbuf, read, write);

    rbuf = 0;
    uint8_t data;
    uart_read(&data);
    TEST_ASSERT_EQUAL(1, rbuf);
    TEST_ASSERT_EQUAL(1, data);
}

void test_uart_write(){
    uart_init(&rbuf, &wbuf, &read, &write);

    wbuf = 0;
    uint8_t data = 14;
    uart_write(data);
    TEST_ASSERT_EQUAL(14, wbuf);
}

void test_uart_transmit(){
    uart_init(&rbuf, &wbuf, &read, &write);

    UCSR0A |= (1<<UDRE0);
    UDR0 = 0;

    TEST_ASSERT_EQUAL(0, UDR0);

    uart_transmit();

    //read from mock buffer always 1
    TEST_ASSERT_EQUAL(1, UDR0);
}

void test_uart_poll(){
    uart_init(&rbuf, &wbuf, &read, &write);

    UCSR0A |= (1<<RXC0);
    UDR0 = 14;
    rbuf = 0;
    UCSR0A |= (1<<UDRE0);

    TEST_ASSERT_EQUAL(0, rbuf);

    uart_poll();

    TEST_ASSERT_EQUAL(14, rbuf);
    //read from mock buffer always 1
    TEST_ASSERT_EQUAL(1, UDR0);
}

void test_uart_receive(){
    uart_init(&rbuf, &wbuf, &read, &write);

    UCSR0A |= (1<<RXC0);
    UDR0 = 14;
    rbuf = 0;
    TEST_ASSERT_EQUAL(0, rbuf);

    uart_receive();
    TEST_ASSERT_EQUAL(14, rbuf);
}
void test_uart_lines_increment(){
    uart_init(&rbuf, &wbuf, &read, &write);

    UCSR0A |= (1<<RXC0);
    UDR0 = '\n';
    rbuf = 0;
    uart_lines = 0;
    uart_receive();
    TEST_ASSERT_EQUAL(1, uart_lines);
}

int newline_read(int *buffer, uint8_t *data) {
    *data = '\n';
    return 0;
}

void test_uart_lines_decrement(){
    uart_init(&rbuf, &wbuf, &newline_read, &write);
    uart_lines = 1;
    uint8_t data = 0;
    uart_read(&data);
    TEST_ASSERT_EQUAL('\n', data);
    TEST_ASSERT_EQUAL(0, uart_lines);
}