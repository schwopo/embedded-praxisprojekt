#include "unity.h"
#include "bootcamp/fastsine.h"
#include <math.h>

const float sine_acceptable_delta = 0.01f;

void test_sine_0() {
    TEST_ASSERT_EQUAL_FLOAT(0, sine(0));
}

void test_sine_pi_multiples() {
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 1, sine(M_PI_2));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(M_PI));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-M_PI));
}

void test_sine_edge_cases() {
    float edge_distance = 0.00000001f;
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(2*M_PI - edge_distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-2*M_PI + edge_distance));
}

void test_sine_big_input() {
    float distance = 20 * 2 * M_PI;
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 1, sine(M_PI_2 + distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(M_PI + distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2 + distance));
    
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, -1, sine(M_PI+M_PI_2 - distance));
    TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, 0, sine(-M_PI + distance));
}

void test_misc() {
    TEST_ASSERT_EQUAL(0, 2.0 - floor(2.0));
}

void test_sine_compare_math_h_sin() {
    float step_size = 0.001f;
    for(float x = 0; x < M_PI; x += step_size){
        TEST_ASSERT_FLOAT_WITHIN(sine_acceptable_delta, sin(x), sine(x));
    }
}