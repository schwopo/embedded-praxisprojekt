import paho.mqtt.client as mqtt
import parse
import threading
import serial

name = "manull"
broker_addr = "134.91.79.29"
port = 1883
serial_path = "/dev/ttyACM0"
led_enabled = False
temp_threshold = 23
thermostat_id = None

def average_temp():
    return sum(temp_dict.values()) / len(temp_dict.values())

def set_led():
    if(average_temp() > temp_threshold):
        s.write("led 1\n".encode('utf-8'))
    else:
        s.write("led 0\n".encode('utf-8'))

def serial_readl():
    while True:
        line_text = s.readline().decode('utf-8')

        print("Serial Output: ", line_text)
        client.publish(name + "/output", line_text)

        r = parse.parse("task id: {} command: {}", line_text)
        if r != None:
            task_id = r[0]
            command = r[1].replace('\n', '')
            print("Command {} has id {}".format(command, task_id))
            if command == "pubtemp":
                global thermostat_id
                thermostat_id = task_id

        if line_text.startswith("!"):
            r = parse.parse("!{} {}", line_text)
            subtopic = r[0]
            value = r[1]
            value = value.replace('\n', '')
            client.publish(name + "/" + subtopic, value)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(name+"/commands")
    client.subscribe(name+"/subscribe_all")
    client.subscribe("enableThermostat")

def on_message(client, userdata, msg):
        r = parse.parse("{}/{}", msg.topic)
        if r != None:
            value = msg.payload.decode('utf-8')
            handle_message(r[0], r[1], value)
        else:
            r = parse.parse("{}", msg.topic)
            value = msg.payload.decode('utf-8')
            handle_message(r[0], None,  value)

def handle_message(user, topic, value):
    global led_enabled
    global temp_dict

    if topic == "commands":
        print("Remote Command for: {}\n--> {} ".format(user, value))
        s.write((value + '\n').encode("utf-8"))
    
    if topic == "temp":
        temperature = int(value)
        temp_dict[user] = temperature
        print("Added to temperatures: {} : {}".format(user, temperature))
        print("Average temperature: {}".format(average_temp()))
        if led_enabled:
            set_led()
    
    if topic == "subscribe_all":
        if value == "1":
            client.subscribe("+/commands")
        if value == "0":
            client.unsubscribe("+/commands")

    if user == "enableThermostat":
        global thermostat_id
        if value == "1" and thermostat_id == None:
            led_enabled = True
            s.write("ta 0 2000 pubtemp\n".encode('utf8'))
            client.subscribe("+/temp")

        if value == "0" and thermostat_id != None:
            led_enabled = False
            s.write("tr {}\n".format(thermostat_id).encode('utf8'))
            thermostat_id = None
            client.unsubscribe("+/temp")
            temp_dict = {}

print("Running MQTT Client V2")
s = serial.Serial(serial_path)
temp_dict = {}
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(broker_addr, port, 60)

mqttthread = threading.Thread(target=client.loop_forever)
mqttthread.start()

readthread = threading.Thread(target=serial_readl)
readthread.start()

readthread.join()
mqttthread.join()