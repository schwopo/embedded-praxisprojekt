#ifndef COMMAND_H
#define COMMAND_H

/*
List all Commands in the command list
writes into buffer
*/
void command_list(char *buffer);

/*
Add a command to the command list
*/
void command_add(char *name, void (*fn) (char *));

/*
Execute command immediately
*/
int command_execute(char *name, char *args);

/*
Execute command that is the first word.
Everything after is argument.
*/
int command_interpret(char *str);

/*
Check if command exists inside the command list
*/
int command_exists(char *name);

/*
Empty the command list
*/
void command_clear();

#endif