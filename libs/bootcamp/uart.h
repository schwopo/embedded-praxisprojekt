#ifndef UART_H
#define UART_H

#include <inttypes.h>

extern volatile int uart_lines;

//initialize uart using buffers for uart read and uart write and inject read and write functions for the buffers
void uart_init(void *readbuf, void *writebuf, int8_t (*readfunc) (void *, uint8_t *),int8_t (*writefunc) (void *, uint8_t));

//read from uart read buffer
int8_t uart_read(uint8_t *);

//write into uart write buffer
int8_t uart_write(uint8_t);

//write one byte of write buffer to uart data register
//read from uart data register and write to read buffer
void uart_poll(void);

//enable interrupts for receive and transmit
void uart_enable_interrupts();

//write one byte of write buffer to uart data register
//ISR for transmit complete
void uart_transmit();

//read from uart data register and write to read buffer
//ISR for receive complete
void uart_receive();


#endif