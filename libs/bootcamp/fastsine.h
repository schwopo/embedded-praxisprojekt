#ifndef FASTSINE_H
#define FASTSINE_H

//approximate sine by interpolating between known values
float sine(float x);

#endif