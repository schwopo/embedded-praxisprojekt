#ifndef TIMER_H
#define TIMER_H

//set up timer0 to trigger an interrupt every 1ms
void setUpTimerForOneMillisecond();

#endif