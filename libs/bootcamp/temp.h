#ifndef TEMP_H
#define TEMP_H

//offset and gain for celsius conversion
extern int temp_ts_offset;
extern int temp_ts_gain;

//initialize internal temperature sensor and ADC
void temp_init();

//get internal temperature converted to celsius using temp_ts_offset and temp_ts_gain
int temp_get();

//convert ADC value to celsius
//mostly useful for testing
int temp_convert_celsius(int reading, int ts_offset, int ts_gain);

//save ADC value on ADC interrupt
void temp_isr();

#endif