#ifndef TASK_QUEUE_H
#define TASK_QUEUE_H
#include <inttypes.h>

/*
maximum number of tasks that are inside the queue at the same time
*/
#define TASK_QUEUE_SIZE 16

/*
task queue time
to be incremented by timer ISR
*/
volatile extern uint64_t task_queue_time;

/*
Add task to task queue.
Returns task id.
*/
int task_queue_add_task(uint64_t start, uint64_t repeat, void (*fn) (void *), void *context);

/*
remove task from task queue
*/
void task_queue_remove_task(int task_id);

/*
check if task should be executed
execute task if it should
*/
void task_queue_poll();

/*
Designate that the task context shall be freed when task is finished.
Useful for when task context is allocated along with the task.
*/
void task_queue_free_context_on_finish(int id);

/*
remove all entries from task queue and reset timer
*/
void task_queue_clear();


#endif