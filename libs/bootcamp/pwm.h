#ifndef PWM_H
#define PWM_H

typedef struct pwm_type pwm_type;
typedef pwm_type* pwm_t;

/*
create pwm data struct
calls on_fn on 0
calls off_fn on compare_value
*/
pwm_t pwm_create(int max, void (*on_fn)(), void (*off_fn)());

/*
set pwm compare value
higher compare value means more time in on state
*/
void pwm_set_compare_value(pwm_t pwm, int compare_value);

/*
increment the pwm
*/
void pwm_step(pwm_t pwm);

/*
Disable the pwm, causing the step function to be without effect
*/
void pwm_stop(pwm_t pwm);

/*
Enable the pwm after pwm_stop()
*/
void pwm_start(pwm_t pwm);

#endif