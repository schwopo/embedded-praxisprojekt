#ifndef MY_AVR_IO_H
#define MY_AVR_IO_H

#ifdef AVR
    #include <avr/io.h>
#else
    #include "io_mock.h"
#endif

#endif