#ifndef INTERRUPT_MOCK_H
#define INTERRUPT_MOCK_H

#include <inttypes.h>

extern uint8_t TCNT0;
extern uint8_t OCR0A;

extern uint8_t TCCR0A;
extern uint8_t TCCR0B;


#define WGM01 1

#define CS01 1
#define CS00 0

extern uint8_t TIMSK0;

#define OCIE0A 1


#endif