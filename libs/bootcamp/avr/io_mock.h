#include <inttypes.h>


//uart io data register
extern uint8_t UDR0;

//baud register rate
extern uint8_t UBRR0H;
extern uint8_t UBRR0L;

//configuration registers
extern uint8_t UCSR0A; 
extern uint8_t UCSR0B; 
extern uint8_t UCSR0C; 

//ADC
extern uint8_t ADMUX;
extern uint8_t ADCSRA;
extern uint8_t ADCW;

//UCSR0A
#define RXC0 7
#define TXC0 6
#define UDRE0 5
#define FE0 4
#define DOR0 3
#define UPE0 2
#define U2X0 1
#define MPCM0 0

//UCSR0B
#define RXCIE0 7
#define TXCIE0 6
#define UDRIE0 5
#define RXEN0 4
#define TXEN0 3
#define UCSZ02 2
#define RXB80 1
#define TXB80 0

//UCSR0C
#define UMSEL01 7
#define UMSEL00 6
#define UPM01 5
#define UPM00 4
#define USBS0 3
#define UCSZ01 2
#define UCSZ00 1
#define UCPOL0 0


//ADCSRA
#define ADEN 7
#define ADSC 6
#define ADATE 5
#define ADIE 3
#define ADPS2 2
#define ADPS1 1

//ADMUX
#define REFS1 7
#define REFS0 6
#define MUX3 3