#ifndef MY_AVR_INTERRUPT_H
#define MY_AVR_INTERRUPT_H

#ifdef AVR
    #include <avr/interrupt.h>
#else
    #include "interrupt_mock.h"
#endif

#endif