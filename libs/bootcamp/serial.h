#ifndef SERIAL_H
#define SERIAL_H

/*
init and enable interrupts
*/
int serial_init();

/*
init without interrupts
*/
int serial_init_no_interrupts();

/*
printf to uart, limited to 80 chars + \n + \0
*/
int serial_printf(char *format_string, ...);

/*
read from buffer till newline
*/
int serial_readline(char *buffer);

/*
would work but avr sscanf clashes with clib sscanf on tests
turned out to be less useful than expected anyway
*/
//int serial_scanf(char *format_string, ...);

#endif